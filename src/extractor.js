const puppeteer = require('puppeteer')
const config = require('../config.json')
const pEachSeries = require('p-each-series')
const fs = require('fs-extra')

const functions = []
const events = []

async function extract () {
  console.log('extracting...')
  const browser = await puppeteer.launch({
    args: ['—headless', '—no-sandbox']
  })
  const page = await browser.newPage()
  await page.goto(config['api.md'])
  const elements = await page.$$('h4')

  const functionsIterator = async (element) => {
    let text = await page.evaluate((element) => element.textContent, element)
    if (!text.startsWith('bot.')) return
    if (!text.includes('(') || !text.includes(')')) return
    text = text.slice(4)
    text = text.split('(')[0]
    functions.push(text)
  }
  const eventsIterator = async (element) => {
    let text = await page.evaluate((element) => element.textContent, element)
    if (!text.startsWith('"')) return
    text = text.split('"')[1]
    events.push(text)
  }
  await pEachSeries(elements, functionsIterator)
  await pEachSeries(elements, eventsIterator)
  await fs.ensureDir('./out')
  await fs.writeJSON('./out/functions.json', functions)
  console.log('Wrote to ./out/functions.json')
  await fs.writeJSON('./out/events.json', events)
  console.log('Wrote to ./out/events.json')
  await browser.close()
  console.log('Done')
}

extract()
